//
//  NetworkLogger.swift
//  CVPTNetwork
//
//  Created by Sơn Bùi on 10/10/2021.
//

import Alamofire

//struct NetworkLogger {
//     static func log(request: URLRequest) {
//
//        print("\n - - - - - - - - - - OUTGOING - - - - - - - - - - \n")
//        defer { print("\n - - - - - - - - - -  END - - - - - - - - - - \n") }
//
//        let urlAsString = request.url?.absoluteString ?? ""
//        let urlComponents = NSURLComponents(string: urlAsString)
//
//        let method = request.httpMethod != nil ? "\(request.httpMethod ?? "")" : ""
//        let path = urlComponents?.path ?? ""
//        let query = urlComponents?.query ?? ""
//        let host = urlComponents?.host ?? ""
//
//        var logOutput = """
//                        \(urlAsString) \n\n
//                        \(method) \(path)?\(query) HTTP/1.1 \n
//                        HOST: \(host)\n
//                        """
//        for (key,value) in request.allHTTPHeaderFields ?? [:] {
//            logOutput += "\(key): \(value) \n"
//        }
//        if let body = request.httpBody {
//            logOutput += "\n \(NSString(data: body, encoding: String.Encoding.utf8.rawValue) ?? "")"
//        }
//
//        print(logOutput)
//    }
//
//    public static func log(_ request: DataRequest, isShow: Bool = true) {
//        guard isShow else { return }
//        request.responseJSON { response in
//            print("\n++++++++++++++++++++++++++++ RESPONSE ++++++++++++++++++++++++++++++++ \n")
//
//            if let statusCode = response.response?.statusCode {
//                print("Status Code: \(statusCode)\n")
//            }
//
//            guard let dict = ParserResponse.parseToDict(response.data) else {
//                print("Unable to parse response data!!!\n")
//                return
//            }
//
//            switch response.result {
//            case .success(let value):
//                print(value)
//            case .failure(let error):
//                print("RESPONSE: \(dict.description)\n")
//                print(error)
//            }
//
//            print("\n++++++++++++++++++++++++++++++++ END ++++++++++++++++++++++++++++++++++ \n")
//        }
//    }
//
//    public static func log(_ urlRequest: URLRequest?, isShow: Bool = true) {
//        print("\n++++++++++++++++++++++++++++ OUTGOING ++++++++++++++++++++++++++++++++ \n")
//
//
//        if let urlStr = urlRequest?.url?.absoluteString {
//            print("URL: \(urlStr)\n")
//        }
//
//        if let method = urlRequest?.httpMethod {
//            print("METHOD: \(method)\n")
//        }
//
//        if let headers = urlRequest?.allHTTPHeaderFields {
//            print("HEADERS: \(headers)\n")
//        }
//
//        if let jsonBody = urlRequest?.httpBody, let jsonStr = String(data: jsonBody, encoding: .utf8) {
//            print("JSON: \(jsonStr)")
//        }
//
//        print("\n++++++++++++++++++++++++++++++++ END ++++++++++++++++++++++++++++++++++ \n")
//    }
//}
//

class NetworkLogger : EventMonitor {
    func requestDidFinish(_ request: Request) {
        print("\n============================= REQUEST ================================\n")
        print(request.cURLDescription())
    }
    
    func requestIsRetrying(_ request: Request) {
        print("\n======================= REQUEST IS RETRYING ==========================\n")
        print(request.cURLDescription())
    }
    
    func request<Value>(_ request: DataRequest, didParseResponse response: DataResponse<Value, AFError>) {
        guard let data = response.data else {
            return
        }
        if let json = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) {
            print("\n============================= REPONSE ================================\n")
            print(json)
        }
    }
}
