//
//  ParserResponse.swift
//  CVPTNetwork
//
//  Created by Sơn Bùi on 10/10/2021.
//

import Foundation
import Alamofire
import RxSwift

public class ParserResponse {
    
    public static func parseToDict(_ data: Data?) -> Parameters? {
        if let data = data,
            let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
            let dict = json as? Parameters {
            return dict
        }
        return nil
    }
    
    static func parse(data: Data?) -> Result<Void, Error> {
        if let dict = parseToDict(data),
           let message = dict["message"] as? String,
           let code = dict["success"] as? Int, let status = dict["status"] as? Int {
            if code == 1 && status == 200 {
                return .success(())
            }
            return .failure(NetworkError.errorMessage(message: message, status: status))
        }
        return .failure(NetworkError.undefined)
    }
}
