//
//  FSResponseModel.swift
//  CVPTNetwork
//
//  Created by Sơn Bùi on 17/10/2021.
//

import Foundation

public struct FSResponseModel<T : Decodable>: Decodable {
    public let status: Int?
    public let success : Bool?
    public let message : String?
    public let errCode : String?
    public let data : T?
    
    enum CodingKeys: String, CodingKey {
        case status
        case success
        case message
        case errCode = "err_code"
        case data
    }
    
    public init(from decoder: Decoder) throws {
        let values = try? decoder.container(keyedBy: CodingKeys.self)
        
        status = try? values?.decode(Int.self, forKey: .status)
        success = try? values?.decode(Bool.self, forKey: .success)
        message = try? values?.decode(String.self, forKey: .message)
        errCode = try? values?.decode(String.self, forKey: .errCode)
        
        data = try? values?.decode(T.self, forKey: .data)
    }
}
