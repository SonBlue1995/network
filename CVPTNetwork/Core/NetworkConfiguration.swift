//
//  NetworkConfiguration.swift
//  CVPTNetwork
//
//  Created by Sơn Bùi on 10/10/2021.
//

public struct NetworkConfiguration {

    let baseURL: URL
    let isShowLog: Bool
    let timeout: TimeInterval
    let cachePolicy: URLRequest.CachePolicy
    
    public init(baseURL: URL,
                timeout: TimeInterval = 30,
                cachePolicy: URLRequest.CachePolicy = .useProtocolCachePolicy,
                isShowLog: Bool = true) {
        self.baseURL = baseURL
        self.timeout = timeout
        self.isShowLog = isShowLog
        self.cachePolicy = cachePolicy
    }
}
