//
//  NetworkError.swift
//  CVPTNetwork
//
//  Created by Sơn Bùi on 10/10/2021.
//

import Alamofire

public enum NetworkError: Error {
    case tokenExpired(message: String)
    case serverError
    case redirection
    case clientError
    case undefined
    case lostInternetConnection
    case errorMessage(message: String, status: Int)
    case other(error: Error)
}

extension NetworkError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .tokenExpired(let message):
            return message
        case .serverError:
            return "Server failure"
        case .redirection:
            return "Redirection"
        case .clientError:
            return "Client error"
        case .undefined:
            return "Undefined"
        case .errorMessage(let message, _):
            return message
        case .other(let error):
            return error.localizedDescription
        case .lostInternetConnection:
            return "lost_internet_connection"
        }
    }
}

//MARK: - Mapping

public protocol NetworkErrorConvertible {
    func asNetworkError() -> NetworkError
}

extension NetworkError: NetworkErrorConvertible {
    public func asNetworkError() -> NetworkError {
        return self
    }
}

public extension Error {
    func asNetworkError() -> NetworkError {
        (self as? NetworkErrorConvertible)?.asNetworkError() ?? .other(error: self)
    }
}

extension AFError: NetworkErrorConvertible {
    public func asNetworkError() -> NetworkError {
        if let error = self.underlyingError as NSError?, error.domain == NSURLErrorDomain {
            if error.code == -1009 {
                return .lostInternetConnection
            }
            return .other(error: error)
        }
        
        return (self.underlyingError as? NetworkErrorConvertible)?.asNetworkError() ?? .other(error: self)
    }
}

