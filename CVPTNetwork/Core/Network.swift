//
//  Network.swift
//  CVPTNetwork
//
//  Created by Sơn Bùi on 10/10/2021.
//

import RxSwift
import Alamofire

public class Network {
    let configuration: NetworkConfiguration
    let session: Session
    
    public init(configuration: NetworkConfiguration) {
        self.configuration = configuration
        self.session = Session(eventMonitors: [NetworkLogger()])
    }
}

extension Network: ReactiveCompatible {}

//MARK: - Request

extension Reactive where Base: Network {
    public func request(_ route: EndPointConvertible) -> Single<DataRequest> {
        return request(RequestConvertible(configuration: base.configuration, endpoint: route))
    }
    
    public func request(_ request: RequestConvertible) -> Single<DataRequest> {
        return Single.create { [session = base.session] callback -> Disposable in
            callback(.success(session.request(request)))
            return Disposables.create()
        }
    }
}

//MARK: - Multipart

extension Reactive where Base: Network {
    public func upload(_ router: UploadEndpointConvertible) -> Single<UploadRequest> {
        return upload(UploadConvertible(configuration: base.configuration, endpoint: router))
    }
    
    public func upload(_ request: UploadConvertible) -> Single<UploadRequest> {
        return Single.create { [session = base.session] callback -> Disposable in
            let uploadRequest = session.upload(multipartFormData: { request.append(to: $0) }, with: request)
            callback(.success(uploadRequest))
            return Disposables.create()
        }
    }
}
