//
//  HTTPEncoder.swift
//  CVPTNetwork
//
//  Created by Sơn Bùi on 10/10/2021.
//

public enum HTTPEncoder {
    case noEncoder
    case urlEncoder(parameters: [String: Any])
    case jsonEncoder(parameters: [String: Any])
    case urlJsonEncoder(jsonParameters: [String: Any], urlParameters: [String: Any])
}

