//
//  UploadEndpointConvertible.swift
//  CVPTNetwork
//
//  Created by Sơn Bùi on 10/10/2021.
//

import Alamofire

public typealias CVPTMultipartFormData = MultipartFormData

public protocol UploadEndpointConvertible: EndPointConvertible {
    var parameters: [String: Any] { get }
    
    func append(to multipartFormData: CVPTMultipartFormData)
}
