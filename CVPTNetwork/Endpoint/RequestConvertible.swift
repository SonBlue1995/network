//
//  RequestConvertible.swift
//  CVPTNetwork
//
//  Created by Sơn Bùi on 10/10/2021.
//

import Alamofire

public struct RequestConvertible: URLRequestConvertible {
    let configuration: NetworkConfiguration
    let endpoint: EndPointConvertible
    
    public func asURLRequest() throws -> URLRequest {
        try endpoint.asURLRequest(configuration: configuration)
    }
}

