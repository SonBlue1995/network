//
//  EndpointConvertible.swift
//  CVPTNetwork
//
//  Created by Sơn Bùi on 10/10/2021.
//

import Alamofire

public typealias EndpointMethod = HTTPMethod

public protocol EndPointConvertible {
    var path: String { get }
    var method: EndpointMethod { get }
    var encoder: HTTPEncoder { get }
    var headers: HTTPHeaders { get }
    
    func asURLRequest(configuration: NetworkConfiguration) throws -> URLRequest
}
 
public extension EndPointConvertible {

    var headers: HTTPHeaders {
        return ["Content-Type": "application/json"]
    }
    
    var parameters: [String: Any] {
        return [:]
    }
    
    func asURLRequest(configuration: NetworkConfiguration) throws -> URLRequest {
        var request = URLRequest(url: configuration.baseURL.appendingPathComponent(path),
                                 cachePolicy: configuration.cachePolicy,
                                 timeoutInterval: configuration.timeout)
        request.httpMethod = method.rawValue
        request.headers = headers
        
        switch encoder {
        case .jsonEncoder(let parameters):
            request = try JSONEncoding.default.encode(request, with: parameters)
        case .urlEncoder(let parameters):
            request = try URLEncoding.default.encode(request, with: parameters)
        case .urlJsonEncoder(let jsonParameters, let urlParameters):
            request = try JSONEncoding.default.encode(request, with: jsonParameters)
            request = try URLEncoding.default.encode(request, with: urlParameters)
        default:
            break
        }
        
        return request
    }
}

