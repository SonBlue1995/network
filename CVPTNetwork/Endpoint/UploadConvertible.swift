//
//  UploadConvertible.swift
//  CVPTNetwork
//
//  Created by Sơn Bùi on 10/10/2021.
//

import Alamofire

public struct UploadConvertible: URLRequestConvertible {
    let configuration: NetworkConfiguration
    let endpoint: UploadEndpointConvertible
    
    public func asURLRequest() throws -> URLRequest {
        try endpoint.asURLRequest(configuration: configuration)
    }
    
    public func append(to multipartFormData: MultipartFormData) {
        endpoint.append(to: multipartFormData)
    }
}
