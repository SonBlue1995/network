//
//  Single+Decodable.swift
//  CVPTNetwork
//
//  Created by Sơn Bùi on 10/10/2021.
//

import RxSwift
import Alamofire

public extension PrimitiveSequenceType where Element: DataRequest, Trait == SingleTrait {

    func decode<T: Decodable>(type: T.Type = T.self, decoder: Alamofire.DataDecoder = JSONDecoder()) -> Single<T> {
        return flatMap { request -> Single<T> in
            return Single.create { single -> Disposable in
                let request = request.responseDecodable(of: type, decoder: decoder) { response in
                    switch response.result {
                    case .success(let value):
                        single(.success(value))
                    case .failure(let error):
                        print(error)
                        single(.failure(error))
                    }
                }
                request.resume()
                return Disposables.create {
                    request.cancel()
                }
            }
        }
    }
}
