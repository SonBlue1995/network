//
//  Single+Validate.swift
//  CVPTNetwork
//
//  Created by Sơn Bùi on 10/10/2021.
//

import RxSwift
import Alamofire

public extension PrimitiveSequenceType where Element: DataRequest, Trait == SingleTrait {

    func validate() -> Single<DataRequest> {
        return map { request -> DataRequest in
            request.validate { (_, response, data) -> DataRequest.ValidationResult in
                switch response.statusCode {
                case 200..<300, 404:
                    return ParserResponse.parse(data: data)
                case 300..<400:
                    return .failure(NetworkError.redirection)
                case 401:
                    return .failure(NetworkError.tokenExpired(message: ""))
                case 400..<500:
                    return .failure(NetworkError.clientError)
                case 500..<600:
                    return .failure(NetworkError.serverError)
                default:
                    return .failure(NetworkError.undefined)
                }
            }
            return request
        }
    }
}
