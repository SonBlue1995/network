# CVPTNetwork

[![CI Status](https://img.shields.io/travis/Sơn Bùi/Calendar.svg?style=flat)](https://travis-ci.org/Sơn Bùi/CVPTNetwork)
[![Version](https://img.shields.io/cocoapods/v/CVPTNetwork.svg?style=flat)](https://cocoapods.org/pods/CVPTNetwork)
[![License](https://img.shields.io/cocoapods/l/CVPTNetwork.svg?style=flat)](https://cocoapods.org/pods/CVPTNetwork)
[![Platform](https://img.shields.io/cocoapods/p/CVPTNetwork.svg?style=flat)](https://cocoapods.org/pods/CVPTNetwork)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Calendar is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CVPTNetwork'
```

## Author

Sơn Bùi, covanphongthuy@gmail.com

## License

CVPTNetwork is available under the MIT license. See the LICENSE file for more info.

