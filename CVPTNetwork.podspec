Pod::Spec.new do |spec|

  spec.name         = "CVPTNetwork"
  spec.version      = "0.0.1"
  spec.summary      = "A short description of CVPTNetwork."
  spec.description  = "Networking"

  spec.homepage     = "https://amduongnguhanh.vn/"

  spec.license      = "Fincheck Vietnam"
  spec.author       = { "Sơn Bùi" => "covanphongthuy@gmail.com" }

  spec.platform     = :ios, "10.0"
  spec.source       = { :git => "https://SonBlue1995@bitbucket.org/SonBlue1995/network.git", :tag => "#{spec.version}" }
  #spec.ios.vendored_frameworks = 'CVPTNetwork.framework'
  spec.ios.deployment_target = '10.0'

  spec.source_files  	= "CVPTNetwork/**/*.{swift,c,m,h}"
  #spec.resources 	= "CVPTNetwork/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"
  
  spec.requires_arc = true
  spec.swift_versions = "5.0"
  
  spec.dependency 'Alamofire'
  spec.dependency 'RxSwift'
  spec.dependency 'RxCocoa'
  spec.dependency 'Swinject'
  spec.dependency 'NSObject+Rx'
  spec.dependency 'RxAppState'

end